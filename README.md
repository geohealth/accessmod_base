# Accessmod Base Images Repository

This repository is dedicated to storing Docker images for `accessmod_base`. These base images are used as foundational layers for building and deploying Accessmod applications.

## Docker Push Example

To push an image to this repository using Docker, follow these steps:

1. **Tag your image**: Before pushing the image, tag it with the repository's URL.
   ```sh
   docker tag accessmod_base:5.7-n registry.gitlab.unige.ch/geohealth/accessmod_base:5.7-n
   ```

2. **Push the image**: After tagging, use the `docker push` command to upload your image to the repository.
   ```sh
   docker push registry.gitlab.unige.ch/geohealth/accessmod_base:5.7-n
   ```

## Skopeo Copy Example

Alternatively, you can use Skopeo to copy images directly from one registry to another:

```sh
skopeo copy docker://source-registry/accessmod_base:5.7-n docker://registry.gitlab.unige.ch/geohealth/accessmod_base:5.7-n
```

Replace `source-registry` with the URL of the source registry and ensure the tag (`5.7-n` in this case) matches your specific image version.
